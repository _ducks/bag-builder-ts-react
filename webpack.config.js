const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const webpack = require('webpack');

const basePath = __dirname;

module.exports = {
  cache: false,
  context: path.resolve(basePath, "src"),
  resolve: {
    extensions: ['.js', '.ts', '.tsx'],
  },
  entry: './index.tsx',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader']
      },
    ],
  },
  output: {
    filename: 'bundle.js',
    path: path.resolve(basePath, 'dist'),
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: 'index.html',
      favicon: '',
    }),
    new MiniCssExtractPlugin({
      filename: "[name].css",
    }),
  ],
  devServer: {
    contentBase: path.resolve(basePath, 'dist'),
    publicPath: '/',
    inline: true,
    hot: true,
  }
};
