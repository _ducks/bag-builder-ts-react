import * as React from 'react';

interface Props {
  name: string,
  title: string,
  value: string,
  placeholder: string,
  options: {
    title: string,
    value: string      
  }[],
  handleSelectChange: React.ChangeEventHandler<HTMLSelectElement>
}

export const Select: React.FC<Props> = (props) => {
  return (
    <div className="form-group">
      <label htmlFor={ props.name }> { props.title }</label>
      <select
        name={ props.name }
        value={ props.value }
        onChange={ props.handleSelectChange }
      >
        <option value="" disabled>{ props.placeholder }</option>
        { props.options.map(option => {
          return (
            <option
              key={ option.title }
              value={ option.value }
              label={ option.title }>{ option.title }
            </option>
          );
        })}
      </select>
    </div>
  )
}
