import * as React from 'react';
import { Input } from './Input';
import { Select } from './Select';

interface Props {
  form: {
    name: string,
    type: string,
    stability: string,
  },
  handleFormSubmit: React.FormEventHandler<HTMLFormElement>,
  handleInputChange: React.ChangeEventHandler<HTMLInputElement>
  handleSelectChange: React.ChangeEventHandler<HTMLSelectElement>
}

export const Form: React.FC<Props> = (props) => {
  const types = [
    { title: 'Putter', value: 'putter' },
    { title: 'Midrange', value: 'midrange' },
    { title: 'Fairway', value: 'fairway' },
    { title: 'Control', value: 'control' },
    { title: 'Distance', value: 'distance' }
  ];

  const stabilities = [
    { title: "Very Overstable", value: "vos" },
    { title: "Overstable", value: "os" },
    { title: "Stable", value: "stable" },
    { title: "Understable", value: "us" },
    { title: "Very Understable", value: "vus" },
  ];

  return (
    <form onSubmit = { props.handleFormSubmit }>
      <Input
        title = { 'Disc Name' }
        value = { props.form.name }
        name = { 'name' }
        placeholder = { 'Add disc name' }
        handleInputChange = { props.handleInputChange }
      >
      </Input>
      <Select
        title = { 'Disc Type' }
        value = { props.form.type }
        name = { 'type' }
        options = { types }
        placeholder = { 'Choose disc type' }
        handleSelectChange = { props.handleSelectChange }
       >
      </Select>
      <Select
        title = { 'Disc Stability' }
        value = { props.form.stability }
        name = { 'stability' }
        options = { stabilities }
        placeholder = { 'Choose disc stability' }
        handleSelectChange = { props.handleSelectChange }
        >
      </Select>
      <button type="submit">Add Disc</button>
    </form>
  )
}
