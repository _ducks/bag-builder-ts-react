import * as React from 'react';

interface Props {
  name: string,
  title: string,
  placeholder: string,
  value: string,
  handleInputChange: React.ChangeEventHandler<HTMLInputElement> 
}

export const Input: React.FC<Props> = (props) => {
  return (
    <div className="form-group">
      <label htmlFor={ props.name }> { props.title }</label>
      <input 
        name={ props.name } 
        placeholder={ props.placeholder }
        value={ props.value }
        onChange={ props.handleInputChange }
      ></input>
    </div>
  )
}
