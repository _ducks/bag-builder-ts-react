import * as React from 'react';

interface Props {
  value: string[]
}

export const Cell: React.FC<Props> = (props) => {
  let rows = [];

  if (props.value) {
    props.value.forEach((v) => (
      rows.push(<p key={ v }>{ v }</p>)
    ));
  }

  return <td className="cell">{ rows }</td>;
}
