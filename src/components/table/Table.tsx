import * as React from 'react';
import { Disc } from '../../models/disc';
import { SelectedDiscs } from '../../models/selectedDiscs';
import { Cell } from './Cells';

interface Props {
  selectedDiscs: SelectedDiscs,
}

interface Rows {
  [key: string]: any,
  putter: Element[],
  midrange: Element[],
  fairway: Element[],
  control: Element[],
  distance: Element[],
}

export const DiscTable: React.FC<Props> = (props) => {
  let rows: Rows = {
    putter: [],
    midrange: [],
    fairway: [],
    control: [],
    distance: []
  };
  
  const { selectedDiscs } = props

  const renderCell = (value) => {
    const disc = value;

    return (
      <Cell 
        value={ disc } />
    )
  }

  for (let k in selectedDiscs) {
    for (let j in selectedDiscs[k]) {
      rows[k].push(renderCell(selectedDiscs[k][j]));
    }
  }

  return (
    <table>
      <thead>
        <tr>
          <th>Bag Matrix</th>
          <th>Very OS</th>
          <th>OS</th>
          <th>Stable</th>
          <th>US</th>
          <th>Very US</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>Putter</th>
          { rows.putter }
        </tr>
        <tr>
          <th>Midrange</th>
          { rows.midrange }
        </tr>
        <tr>
          <th>Fairway</th>
          { rows.fairway }
        </tr>
        <tr>
          <th>Control</th>
          { rows.control }
        </tr>
        <tr>
          <th>Distance</th>
          { rows.distance }
        </tr>
      </tbody>
    </table>
  );
};
