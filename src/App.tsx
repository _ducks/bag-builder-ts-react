import * as React from 'react';
import { DiscTable } from './components';
import { Form } from './components';
import state from './state.json';
import { SelectedDiscs } from './models/selectedDiscs';

import './App.css';

export const App = () => {
  const [selectedDiscs, setSelectedDiscs] = 
    React.useState<SelectedDiscs>(state.selectedDiscs);

  const [form, setForm] = 
    React.useState<{ 
      name?: string,
      type?: string,
      stability?: string
    }>(state.form);

  const SelectedDiscsProps = {
    selectedDiscs: selectedDiscs,
  };

  const handleFormSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    const name = form.name;
    const type = form.type;
    const stability = form.stability;

    console.log(form);

    if (!name || !type || !stability) {
      event.preventDefault();
      return;
    };

    setSelectedDiscs(prev => {
      console.log(prev);
      
      return { 
        ...prev,
        [type]: {
          ...prev[type],
          [stability]: [
            ...prev[type][stability],
            name
          ],
        },
      }
    });

    setForm({ name: '', type: '', stability: ''});

    event.preventDefault();
  }

  // TODO: make one generic function for both input and select
  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const name = event.target.name;
    const value = event.target.value;

    setForm(prev => {
      return {
        ...prev,
        [name]: value,
      };
    });
  }

  const handleSelectChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    const name = event.target.name;
    const value = event.target.value;

    setForm(prev => {
      return {
        ...prev,
        [name]: value,
      };
    });
  }

  return (
    <div className="">
      <DiscTable { ...SelectedDiscsProps } />
      <Form 
        form={ form } 
        handleFormSubmit={ handleFormSubmit }
        handleInputChange={ handleInputChange } 
        handleSelectChange={ handleSelectChange }
      />
    </div>
  );
}
