export interface Disc {
  name: string,
  type: DiscType,
  stability: DiscStability
}

enum DiscType {
  Putter,
  Midrange,
  Fairway,
  Control,
  Distance
}

enum DiscStability {
  VeryOverstable,
  Overstable,
  Stable,
  Understable,
  VeryUnderstable,
}
