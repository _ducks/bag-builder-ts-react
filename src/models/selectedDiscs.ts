export interface SelectedDiscs {
  [key: string]: SelectedDiscType,
  putter: SelectedDiscType,
  midrange: SelectedDiscType,
  fairway: SelectedDiscType,
  control: SelectedDiscType,
  distance: SelectedDiscType,
}

interface SelectedDiscType {
  vos: string[],
  os: string[],
  stable: string[],
  us: string[],
  vus: string[], 
}
